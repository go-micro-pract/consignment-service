module gitlab.com/go-micro-pract/consignment-service

go 1.12

require (
	github.com/golang/protobuf v1.3.1
	github.com/micro/go-micro v1.7.0
	gitlab.com/go-micro-pract/vessel-service v0.0.0-20190625154409-89ef4a0f3581
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859
)
