# consignment-service/Dockerfile

# We use the official golang image, which contains all the
# correct build tools and libraries. Notice `as builder`,
# this gives this container a name that we can reference later on.
FROM ubuntu:latest

RUN mkdir -p /app/service-consignment

# Set our workdir to our current service in the gopath
WORKDIR /app/service-consignment

# Copy the current code into our workdir
COPY service-consignment .

CMD ["./service-consignment"]